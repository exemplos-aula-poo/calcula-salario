

public class Salario
{
    private double horasTrabalhadas, salarioHora;
    private int numDependentes;
    
    public Salario() {
    }
    
    public Salario(double horasTrabalhadas, double salarioHora, int numDependentes) {
        this.horasTrabalhadas = horasTrabalhadas;
        this.salarioHora = salarioHora;
        this.numDependentes = numDependentes;
    }
    
    public double salarioLiquido() {
        return salarioBruto() - descontoInss() - descontoIR();   
    }
    
    public double salarioBruto() {
        return this.horasTrabalhadas * this.salarioHora + (50 * this.numDependentes);
    }

    public double descontoInss() {
        return (this.salarioBruto() <= 1000)? this.salarioBruto() * 0.085 : this.salarioBruto() *  0.09; 
    }
    
    public double descontoIR() {
        if(this.salarioBruto() <= 500){
            return 0;   
        } else if(this.salarioBruto() > 500 && this.salarioBruto() <= 1000) {
            return this.salarioBruto() * 0.05;   
        } else {
            return this.salarioBruto() * 0.07;   
        }
    }
    
    public void setHorasTrabalhadas(double horasTrabalhadas) {
        this.horasTrabalhadas = horasTrabalhadas;
    }
    
    public double getHorasTrabalhadas() {
        return this.horasTrabalhadas;   
    }
    
    public void setSalarioHora(double salarioHora) {
        this.salarioHora = salarioHora;   
    }
    
    public double getSalarioHora(double salarioHora) {
        return this.salarioHora;   
    }
    
    public void setNumDependentes(int numDependentes) {
        if(numDependentes >= 0) {
            this.numDependentes = numDependentes;
        }
    }
    
    public int getNumDependentes() {
        return this.numDependentes;   
    }
}
