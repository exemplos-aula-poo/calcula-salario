import java.util.Scanner;


public class Principal
{
    public static void main(String[] args) {
        
        Scanner le = new Scanner(System.in);
        
        Salario salario1 = new Salario();
        
        System.out.print("Informe as horas trabalhadas: ");
        salario1.setHorasTrabalhadas(le.nextDouble());
        System.out.print("Informe o valor da hora trabalhada: ");
        salario1.setSalarioHora(le.nextDouble());
        System.out.print("Informe o número de dependentes: ");
        salario1.setNumDependentes(le.nextInt());
        
        System.out.print("Total do salario líquido: " + salario1.salarioLiquido());
    }
}
